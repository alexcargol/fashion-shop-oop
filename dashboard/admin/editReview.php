<?php
include "../../config.php";

$review = Entity\Review::find(intval($_GET['id']));
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>WEB-04 ONLINE-SHOP</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
</head>
<body>
<?php include 'topbar.php';?>
<div id="content" class="container "  >

    <div id="menu" class="col-12 row menu ">
        <?php include 'admin_header.php'; ?>
    </div>

    <div id="body">


        <div id="fullcolor" class="col-12" >

            <form action="processEditReview.php?id=<?php echo $_GET['id']; ?>" method="post">
                <div class="form-row">
                    <label for="user">Username</label>
                    <input class="form-control" id="user" type="text" value="<?php echo $review->getUser();?>" name="user">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Please rate this product!</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"><?php echo $review->getDescription();?></textarea>
                </div>
                <h6>Please rate this product</h6>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio1"
                           value="1">
                    <label class="form-check-label" for="inlineRadio1">1</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio2"
                           value="2">
                    <label class="form-check-label" for="inlineRadio2">2</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio3"
                           value="3">
                    <label class="form-check-label" for="inlineRadio3">3</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio4"
                           value="4">
                    <label class="form-check-label" for="inlineRadio4">4</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio5"
                           value="5">
                    <label class="form-check-label" for="inlineRadio5">5</label>
                </div>
                <div class="form-row">
                    <label for="email">Email</label>
                    <input class="form-control" id="email" type="text" value="<?php echo $review->getEmail();?>" name="email">
                </div>
                <div class="form-row">
                    <label for="prod_id">Product Id</label>
                    <input class="form-control" id="prod_id" type="text" value="<?php echo $review->getProdId();?>" name="prod_id">
                </div>
                <div class="form-row">
                    <label for="status">Status</label>
                    <input class="form-control" id="status" type="text" value="<?php echo $review->getStatus();?>" name="status" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>


        </div>
    </div>
</div>
</body>
</html>




