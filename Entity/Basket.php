<?php
namespace Entity;

class Basket extends Base
{
    public $user_id;

    /**
     * @return mixed
     */
    public static function tableName(){
        return 'basket';
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Basket
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Basket
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     * @return Basket
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return Basket
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayType()
    {
        return $this->pay_type;
    }

    /**
     * @param mixed $pay_type
     * @return Basket
     */
    public function setPayType($pay_type)
    {
        $this->pay_type = $pay_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Basket
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public $product_id;

    public $quantity;

    public $pay_type;

    public $date;

    public function getBaskets()
    {
        $data = query("SELECT * FROM basket ORDER by id DESC");
        $list = [];

        foreach ($data as $dbLine){
            $list[] = Basket::find($dbLine['id']);
        }

        return $list;
    }

}