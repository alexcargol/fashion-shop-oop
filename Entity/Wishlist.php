<?php
namespace Entity;

class Wishlist extends Base
{
    /**
     * @return mixed
     */


    public static function tableName(){
        return 'wishlist';
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Wishlist
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

public $user_id;
public $product_id;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Wishlist
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     * @return Wishlist
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * Wishlist constructor
     * @param $user_id
     * @param $product_id
     */


}