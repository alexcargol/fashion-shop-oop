<?php
namespace Entity;

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 1/6/2020
 * Time: 8:39 PM
 */
class Category extends Base
{

    public $name;

    /**
     * @return mixed
     */

    public static function tableName(){
        return 'category';
    }
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return Product[]
     */
    public function getProducts()
    {
        $data = query("SELECT * FROM product WHERE category_id=".intval($this->id));
        $list = [];

        foreach ($data as $dbLine){
            $list[] = Product::find($dbLine['id']);
        }

        return $list;
    }

    public function getProductsForIndex()
    {
        $data = query("SELECT * FROM product WHERE category_id='".intval($this->id)."' LIMIT 4");
        $list = [];

        foreach ($data as $dbLine){
            $list[] = Product::fromArray($dbLine);
        }

        return $list;
    }
}