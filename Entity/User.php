<?php
namespace Entity;
/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 1/10/2020
 * Time: 8:21 PM
 */
class User extends Base
{

    public $username;

    public $email;

    public $password;

    public $fullname;

    public $phone;

    public $address;

    public $city;

    public $zip;

    public $wishlist;

    /**
     * @return mixed
     */

    public static function tableName(){
        return 'user';
    }
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     * @return User
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWishlist()
    {
        $listid ='';
        $list=[];

        $data= query('SELECT * FROM wishlist WHERE user_id='.intval($this->getId()));
        $listid = $data[0]["product_id"];
        $list = explode(',', $listid);
        return $list;
        }

    public function getOrders()
    {
        $data = query("SELECT * FROM orders WHERE user_id=".intval($this->id));
        $list = [];

        foreach ($data as $dbLine){
            $list[] = Orders::find($dbLine['id']);
        }

        return $list;
    }

        public function getWishlistId()
        {
            $Wishlistid = query('SELECT id FROM wishlist WHERE user_id='.intval($this->getId()));
            $Wishlistid = $Wishlistid [0]["id"];
            return $Wishlistid;
        }


    public function addtoWishlist($productId)
    {
        $list = $this->getWishlist();
        $list[] = $productId;
        return $list;

    }

    /**
     * @param mixed $wishlist
     * @return User
     */
    public function setWishlist($wishlist)
    {
        $this->wishlist = $wishlist;
        return $this;
    }



}