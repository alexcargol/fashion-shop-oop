<?php
namespace Entity;

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1/9/2020
 * Time: 10:43 AM
 */
class Product extends Base
{

    public $title;

    public $price;

    public $discount;

	public $photo; 
	
    public $description;

    public $size;

    public $gender;

    public $featured;

    public $category_id;

    /**
     * @return mixed
     */

    public static function tableName(){
        return 'product';
    }
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return Product
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return Product
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     * @return Product
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return Product
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * @param mixed $featured
     * @return Product
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     * @return Product
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }

    /**
     * @return mixed
     */



    public function getFinalPrice()
    {
        return round((100-$this->discount)*$this->price/100, 2);
    }

    public function getCategory()
    {
        return new Category($this->category_id);
    }
    public function getReview()
    {
        return Review::findBy(($this->getId()));
    }

    public function getSimilarProducts()
    {   $list=[];
        $similar=query("SELECT * FROM product WHERE category_id='".intval($this->category_id)."' AND id !='".intval($this->id)."'");
       foreach ($similar as $product)
        {
        $list[]= Product::find($product['id']);
        }
       return $list;
    }

    public function getAllProducts(){

        $data = query("SELECT * FROM product ORDER by id DESC");
        $list = [];

        foreach ($data as $dbLine){
            $list[] = new Product($dbLine['id']);
        }

        return $list;

    }

    public function getDiscountedProducts(){

        $data = query("SELECT * FROM product WHERE discount>0 ORDER BY id DESC");
        $list = [];

        foreach ($data as $dbLine){
            $list[] = Product::find($dbLine['id']);
        }

        return $list;

    }

    public function getLatestProducts(){

        $data = query("SELECT * FROM product ORDER BY id DESC LIMIT 63");
        $list = [];

        foreach ($data as $dbLine){
            $list[] = Product::find($dbLine['id']);
        }

        return $list;

    }

    public function getReviews()
    {
        $data = query("SELECT id FROM review WHERE prod_id=" . intval($this->id));
        $result = [];
        foreach ($data as $reviewData) {
            $result[] = Review::find(($reviewData['id']));
        }
        return $result;
    }

}